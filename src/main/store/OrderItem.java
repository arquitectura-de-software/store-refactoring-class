package store;

public class OrderItem {
	
	private Product product;
	private int quantity;

	/*
	 * Order Item Constructor
	 */
	public OrderItem(Product product, int quantity) {
		this.product = product;
		this.quantity = quantity;
	}
	
	public Product getProduct() {
		return product;
	}

	public int getQuantity() {
		return quantity;
	}


	private DiscountCalculator createDiscountCalculator() {
		DiscountCalculator discountcalculator = null;
		if (getProduct().getCategory() == ProductCategory.Accessories) {
			discountcalculator= new AccessoriesDiscount();			
		}
		if (getProduct().getCategory() == ProductCategory.Bikes) {
			discountcalculator= new BikesDiscount();			
		}
		if (getProduct().getCategory() == ProductCategory.Cloathing) {
			discountcalculator= new CloathingDiscount();			
		}
		return discountcalculator;
	}
	
	float calculateTotalItemFor() {
		float totalItem = 0;
		float discount=0;
		DiscountCalculator discountCalculator = createDiscountCalculator();
		discount =  discountCalculator.calculateDiscount(this);
		totalItem = calculateTotalAmount() - discount;
		return totalItem;
	}

	private float calculateTotalAmount() {
		float itemAmount = getProduct().getUnitPrice() * getQuantity();
		return itemAmount;
	}
}
