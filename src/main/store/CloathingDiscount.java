package store;

public class CloathingDiscount implements DiscountCalculator{

	@Override
	public float calculateDiscount(OrderItem item) {
		float discount = 0;
		if (item.getQuantity() > 2) {
			discount = item.getProduct().getUnitPrice();
		}
		return discount;
	}

}
